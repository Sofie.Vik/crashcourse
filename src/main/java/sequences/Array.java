package sequences;

import java.util.ArrayList;

public class Array {

	public static void main(String[] args) {
		int[] integerArray1 = {5, 5, 5, 5, 5, 5};
		int[] integerArray2 = new int[6];
		
		int myInt = integerArray1[0];
		System.out.println(myInt);
		
		integerArray1[0] = 4;
		System.out.println(integerArray1[0]);
		
		
		for (int i = 0; i < 6; i++) {
			integerArray2[i] = 5;
		}
		
		
		
		
		ArrayList<Integer> integerArray = new ArrayList<Integer>();
		for (int i = 0; i < 20; i++) {
			integerArray.add(i);
		}
		System.out.println(integerArray);
	}
}
